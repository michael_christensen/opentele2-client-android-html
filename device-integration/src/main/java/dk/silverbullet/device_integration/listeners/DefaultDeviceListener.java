package dk.silverbullet.device_integration.listeners;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Set;
import java.util.regex.Pattern;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.exceptions.DeviceInitialisationException;
import dk.silverbullet.device_integration.protocols.bluetooth_smart.BluetoothLowEnergyService;
import dk.silverbullet.device_integration.protocols.bluetooth_smart.GattConstants;
import dk.silverbullet.device_integration.protocols.continua.ContinuaDeviceController;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;

/**
 * Implements the default behavior for the DeviceListener and ContinuaListener
 * interfaces.
 */
public class DefaultDeviceListener implements DeviceListener {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(DefaultDeviceListener.class);

    private boolean connectedViaBluetoothLowEnergy = false;
    private final Activity activity;
    private final OnEventListener onEventListener;
    private ContinuaDeviceController continuaDeviceController;

    // --*-- Constructors --*--

    public DefaultDeviceListener(Activity activity, OnEventListener onEventListener) {
        this.activity = activity;
        this.onEventListener = onEventListener;
    }

    // --*-- Methods --*--

    // -*- DeviceListener methods -*-

    @Override
    public void start() {
        throw new RuntimeException("Should not call DefaultDeviceListener:start.");
    }

    @Override
    public void stop() {
        Log.d(TAG, "Stop!");

        if (connectedViaBluetoothLowEnergy) {
            Intent stopBLEService = new Intent(activity, BluetoothLowEnergyService.class);
            activity.stopService(stopBLEService);
        } else if (continuaDeviceController != null) {
            continuaDeviceController.close();
        } else {
            Log.w(TAG, "stop() called, but no bluetooth connections have been made");
        }
    }

    @Override
    public void connecting() {
        Log.d(TAG, "Connected!");
        sendStatusEvent(JSONConstants.INFO, JSONConstants.CONNECTING);
    }

    @Override
    public void connected() {
        Log.d(TAG, "Connected!");
        sendStatusEvent(JSONConstants.INFO, JSONConstants.CONNECTED);
    }

    @Override
    public void disconnected() {
        Log.d(TAG, "Disconnected!");
        sendStatusEvent(JSONConstants.INFO, JSONConstants.DISCONNECTED);
    }

    @Override
    public void permanentProblem() {
        Log.d(TAG, "Permanent problem!");
        sendStatusEvent(JSONConstants.ERROR, JSONConstants.PERMANENT_PROBLEM);
    }

    @Override
    public void temporaryProblem() {
        Log.d(TAG, "Temporary problem!");
        sendStatusEvent(JSONConstants.ERROR, JSONConstants.TEMPORARY_PROBLEM);
    }

    public void sendStatusEvent(final String type, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject event = createStatusEvent(type, message);
                    onEventListener.onEvent(event);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void measurementReceived(DeviceInformation deviceInformation, Object measurement) {
        throw new RuntimeException("Should not call DefaultDeviceListener:measurementReceived.");
    }

    // -*- Helper methods -*-

    public JSONObject createStatusEvent(String type,
                                        String message) throws JSONException {

        JSONObject event = new JSONObject();
        Date eventTimestamp = new Date();
        event.put(JSONConstants.TIMESTAMP, eventTimestamp.toString());
        event.put(JSONConstants.TYPE, JSONConstants.STATUS);
        JSONObject status = new JSONObject();
        status.put(JSONConstants.TYPE, type);
        status.put(JSONConstants.MESSAGE, message);
        event.put(JSONConstants.STATUS, status);
        return event;
    }

    public JSONObject createDeviceObject(DeviceInformation deviceInformation) throws JSONException {
        JSONObject deviceJSON = new JSONObject();
        if (deviceInformation != null) {
            deviceJSON.put(JSONConstants.MODEL, deviceInformation.getModel());
            deviceJSON.put(JSONConstants.FIRMWARE_REVISION, deviceInformation.getFirmwareRevision());
            deviceJSON.put(JSONConstants.EUI_64, deviceInformation.getEui64());
            deviceJSON.put(JSONConstants.MANUFACTURER, deviceInformation.getManufacturer());
            deviceJSON.put(JSONConstants.SERIAL_NUMBER, deviceInformation.getSerialNumber());
            deviceJSON.put(JSONConstants.SYSTEM_ID, deviceInformation.getSystemId());
        }
        return  deviceJSON;
    }

    public void connect(ContinuaDeviceController continuaDeviceController,
                        BroadcastReceiver broadcastReceiver,
                        int bluetoothClass,
                        Pattern deviceAddressPattern,
                        Pattern deviceNamePattern) {

        final BluetoothManager bluetoothManager =
                (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Log.e(TAG, "Bluetooth is not enabled");
            return;
        }

        boolean bluetoothLowEnergySupported = activity.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        Log.d(TAG, "Bluetooth Low Energy (BLE) Supported: " + bluetoothLowEnergySupported);
        Set<BluetoothDevice> bluetoothDevices = bluetoothAdapter.getBondedDevices();

        searchForCompatibleBluetoothDevice(continuaDeviceController, broadcastReceiver,
                bluetoothClass, deviceAddressPattern, deviceNamePattern, bluetoothDevices);
    }

    private void searchForCompatibleBluetoothDevice(ContinuaDeviceController continuaDeviceController,
                                                    BroadcastReceiver broadcastReceiver,
                                                    int bluetoothClass,
                                                    Pattern deviceAddressPattern,
                                                    Pattern deviceNamePattern,
                                                    Set<BluetoothDevice> bluetoothDevices) {

        Log.d(TAG, "bluetoothClass: " + bluetoothClass);
        Log.d(TAG, "deviceAddressPattern: " + deviceAddressPattern);
        Log.d(TAG, "deviceNamePattern: " + deviceNamePattern.toString());

        for (BluetoothDevice device : bluetoothDevices) {
            String deviceAddress = device.getAddress();
            String deviceName = device.getName();
            int deviceBluetoothClass = device.getBluetoothClass().getMajorDeviceClass();
            int deviceBluetoothType = device.getType();
            Log.d(TAG, "Address: " + deviceAddress);
            Log.d(TAG, "Name: " + deviceName);
            Log.d(TAG, "Bluetooth class: " + deviceBluetoothClass);
            Log.d(TAG, "Bluetooth type: " + deviceBluetoothType);

            // The check "deviceBluetoothClass == bluetoothClass" fails with the new A&D BLE device.
            if (deviceAddressPattern.matcher(deviceAddress).find() &&
                    deviceNamePattern.matcher(deviceName).find()) {
                Log.d(TAG, "Device found at address: " + deviceAddress);

                switch (deviceBluetoothType) {
                    case BluetoothDevice.DEVICE_TYPE_UNKNOWN:
                        Log.e(TAG, "Device has unknown bluetooth type, search continues.");
                        continue;
                    case BluetoothDevice.DEVICE_TYPE_CLASSIC:
                        Log.d(TAG, "Device has classic bluetooth type, connecting with Continua.");
                        connectWithContinua(continuaDeviceController,
                                deviceAddressPattern, deviceNamePattern);
                        return;
                    case BluetoothDevice.DEVICE_TYPE_LE:
                        Log.d(TAG, "Device has low energy bluetooth type, connecting with BLE.");
                        connectWithBluetoothLowEnergy(broadcastReceiver, deviceAddress);
                        return;
                    case BluetoothDevice.DEVICE_TYPE_DUAL:
                        Log.d(TAG, "Device has both classic and low energy bluetooth type, connecting with BLE.");
                        connectWithBluetoothLowEnergy(broadcastReceiver, deviceAddress);
                        return;
                }
            }
        }
        Log.e(TAG, "No compatible bluetooth device found!");
    }

    private void connectWithBluetoothLowEnergy(BroadcastReceiver broadcastReceiver,
                                               String deviceAddress) {
        connectedViaBluetoothLowEnergy = true;
        activity.registerReceiver(broadcastReceiver, makeGattUpdateIntentFilter());

        Intent bleIntent = new Intent(activity, BluetoothLowEnergyService.class);
        bleIntent.putExtra(GattConstants.EXTRA_DATA_DEVICE_ADDRESS, deviceAddress);
        bleIntent.putExtra(GattConstants.EXTRA_DATA_SERVICE_ID,
                GattConstants.WEIGHT_SCALE_SERVICE);
        bleIntent.putExtra(GattConstants.EXTRA_DATA_CHARACTERISTIC_ID,
                GattConstants.WEIGHT_MEASUREMENT_CHARACTERISTIC);
        activity.startService(bleIntent);
    }

    private void connectWithContinua(ContinuaDeviceController continuaDeviceController,
                                     Pattern deviceAddressPrefixPattern,
                                     Pattern deviceNamePattern) {
        this.continuaDeviceController = continuaDeviceController;

        try {
            continuaDeviceController.initiate(deviceNamePattern, deviceAddressPrefixPattern);
        } catch (DeviceInitialisationException e) {
            Log.e(TAG, "Failed to connect with Continua: " + e.getMessage());
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(GattConstants.CONNECTED);
        intentFilter.addAction(GattConstants.DISCONNECTED);
        intentFilter.addAction(GattConstants.SERVICES_DISCOVERED);
        intentFilter.addAction(GattConstants.DATA_AVAILABLE);
        return intentFilter;
    }

}

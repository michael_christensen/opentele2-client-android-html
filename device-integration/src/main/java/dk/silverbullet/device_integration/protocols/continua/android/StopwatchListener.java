package dk.silverbullet.device_integration.protocols.continua.android;

public interface StopwatchListener {
    void timeout();
}

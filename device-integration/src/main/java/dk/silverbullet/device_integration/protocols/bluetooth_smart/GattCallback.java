package dk.silverbullet.device_integration.protocols.bluetooth_smart;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.util.Log;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andbloodpressure.BloodPressureAndPulse;
import dk.silverbullet.device_integration.devices.andweightscale.Weight;

public class GattCallback extends BluetoothGattCallback {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(GattCallback.class);

    private final BluetoothLowEnergyService bleService;
    private BluetoothGattCharacteristic notifyCharacteristic = null;
    private final UUID serviceId;
    private final UUID characteristicId;

    // --*-- Constructors --*--

    public GattCallback(BluetoothLowEnergyService bleService, UUID serviceId,
                        UUID characteristicId) {
        this.bleService = bleService;
        this.serviceId = serviceId;
        this.characteristicId = characteristicId;
    }

    // --*-- Methods --*--

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        Log.d(TAG, "onConnectionStateChanged");
        String intentAction;

        if (newState == BluetoothProfile.STATE_CONNECTED) {

            intentAction = GattConstants.CONNECTED;
            bleService.setConnectionState(GattConstants.STATE_CONNECTED);

            broadcastUpdate(intentAction);
            Log.i(TAG, "Connected to GATT server.");
            // Attempts to discover services after successful connection.
            Log.i(TAG, "Attempting to start service discovery:" +
                    bleService.getBluetoothGatt().discoverServices());

        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            intentAction = GattConstants.DISCONNECTED;
            bleService.setConnectionState(GattConstants.STATE_DISCONNECTED);
            Log.i(TAG, "Disconnected from GATT server.");
            broadcastUpdate(intentAction);

        } else {
            Log.d(TAG, "State changed: " + newState + ", status " + status);
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        Log.d(TAG, "onServicesDiscovered");
        if (status == BluetoothGatt.GATT_SUCCESS) {

            for (BluetoothGattService gattService : bleService.getSupportedGattServices()) {
                UUID aServiceUUID = gattService.getUuid();
                Log.d(TAG, "Service Id: " + aServiceUUID);

                if (aServiceUUID.equals(serviceId)) {
                    Log.d(TAG, "Found weight scale service!");

                    for (BluetoothGattCharacteristic characteristic : gattService.getCharacteristics()) {
                        UUID aCharacteristicUUID = characteristic.getUuid();
                        Log.d(TAG, "Characteristic UUID: " + aCharacteristicUUID);

                        if (aCharacteristicUUID.equals(characteristicId)) {
                            Log.d(TAG, "Found weight measurement characteristic!");

                            final int characteristicProperties = characteristic.getProperties();
                            if ((characteristicProperties | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                                Log.d(TAG, "Characteristic is readable");
                                // If there is an active notification on a characteristic, clear
                                // it first so it doesn't update the data field on the user interface.
                                if (notifyCharacteristic != null) {
                                    bleService.setCharacteristicNotification(
                                            notifyCharacteristic, false);
                                    notifyCharacteristic = null;
                                }
                                bleService.readCharacteristic(characteristic);
                            }
                            if ((characteristicProperties | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                                Log.d(TAG, "Characteristic supports notification");
                                notifyCharacteristic = characteristic;
                                bleService.setCharacteristicNotification(
                                        characteristic, true);
                            }


                        }

                    }
                }
            }

            broadcastUpdate(GattConstants.SERVICES_DISCOVERED);
        } else {
            Log.w(TAG, "onServicesDiscovered received: " + status);
        }
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic,
                                     int status) {
        Log.d(TAG, "onCharacteristicRead");
        if (status == BluetoothGatt.GATT_SUCCESS) {
            broadcastUpdate(GattConstants.DATA_AVAILABLE, characteristic);
        }
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt,
                                        BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, "onCharacteristicChanged");
        broadcastUpdate(GattConstants.DATA_AVAILABLE, characteristic);
    }

    // -*- Broadcasting methods -*-

    private void broadcastUpdate(final String action) {
        Log.d(TAG, "broadcast: " + action);
        final Intent intent = new Intent(action);
        bleService.sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, "broadcast: " + action + " with characteristic " + characteristic.getUuid());
        final Intent intent = new Intent(action);

        Log.d(TAG, "Characteristic bytes: " + Arrays.toString(characteristic.getValue()));
        if (characteristic.getUuid().equals(GattConstants.WEIGHT_MEASUREMENT_CHARACTERISTIC)) {
            Log.d(TAG, "Found weight measurement");
            Weight weight = parseWeight(characteristic);
            Log.d(TAG, "Weight read from characteristic: " + weight);
            intent.putExtra(GattConstants.EXTRA_DATA, weight);
        } else {
            Log.d(TAG, "Found something else");
            // For all other profiles, writes the data formatted in HEX.
            final byte[] data = characteristic.getValue();
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);
                for(byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
                intent.putExtra(GattConstants.EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            }
        }
        bleService.sendBroadcast(intent);
    }

    // TODO: Move to AndWeightScaleBroadcastReceiver
    private Weight parseWeight(BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, "Actual bytes: " + Arrays.toString(characteristic.getValue()));

        int flag = characteristic.getProperties();
        Weight.Unit unit;

        if ((flag & 0x01) != 0) {
            unit = Weight.Unit.LBS;
            Log.d(TAG, "weight format in pounds.");
        } else {
            unit = Weight.Unit.KG;
            Log.d(TAG, "weight format in kilograms.");
        }

        if ((flag & 0x02) != 0) {
            Log.d(TAG, "Timestamp present.");
            int dateOffset = 3;
            Date date = parseDate(characteristic, dateOffset);
            Log.d(TAG, date.toString());
        } else {
            Log.d(TAG, "Timestamp not present.");
        }

        int weightFormat = BluetoothGattCharacteristic.FORMAT_UINT16;
        int weightOffset = 1;
        float weightValue = (float) characteristic.getIntValue(weightFormat, weightOffset);

        Weight weight = new Weight(weightValue, unit);
        Log.d(TAG, String.format("Received weight: %s", weight.toString()));
        return weight;
    }

    // TODO: Move to AndBloodPressureBroadcastReceiver
    private BloodPressureAndPulse parseBloodPressure(BluetoothGattCharacteristic characteristic) {
        Log.d(TAG, "Actual bytes: " + Arrays.toString(characteristic.getValue()));

        int flag = characteristic.getProperties();
        if ((flag & 0x01) != 0) {
            Log.d(TAG, "blood pressure format in kPa, change it to mmHg.");
        } else {
            Log.d(TAG, "blood pressure format in mmHg.");
        }

        if ((flag & 0x02) != 0) {
            Log.d(TAG, "Timestamp present.");
            int dateOffset = 5;
            Date date = parseDate(characteristic, dateOffset);
            Log.d(TAG, date.toString());
        } else {
            Log.d(TAG, "Timestamp not present.");
        }

        int systolicFormat = BluetoothGattCharacteristic.FORMAT_SFLOAT;
        int systolicOffset = 1;
        int systolicValue = Math.round(characteristic.getFloatValue(systolicFormat, systolicOffset));

        int diastolicFormat = BluetoothGattCharacteristic.FORMAT_SFLOAT;
        int diastolicOffset = 2;
        int diastolicValue = Math.round(characteristic.getFloatValue(diastolicFormat, diastolicOffset));

        int meanArterialPressureFormat = BluetoothGattCharacteristic.FORMAT_SFLOAT;
        int meanArterialPressureOffset = 3;
        int meanArterialPressureValue = Math.round(characteristic.getFloatValue(
                meanArterialPressureFormat, meanArterialPressureOffset));

        int pulseFormat = BluetoothGattCharacteristic.FORMAT_SFLOAT;
        int pulseOffset = 12;
        int pulseValue = Math.round(characteristic.getFloatValue(pulseFormat, pulseOffset));

        BloodPressureAndPulse bloodPressureAndPulse = new BloodPressureAndPulse(
                systolicValue, diastolicValue, meanArterialPressureValue, pulseValue);
        Log.d(TAG, String.format("Received blood pressure and pulse: %s", bloodPressureAndPulse.toString()));
        return bloodPressureAndPulse;

    }

    // TODO: Move to DefaultBroadcastReceiver?
    public Date parseDate(BluetoothGattCharacteristic characteristic, int baseOffset) {

        int uInt16Format = BluetoothGattCharacteristic.FORMAT_UINT16;
        int uInt8Format = BluetoothGattCharacteristic.FORMAT_UINT8;

        int yearOffset = baseOffset;
        int monthOffset = 2 + baseOffset;
        int dayOffset = 3 + baseOffset;
        int hourOffset = 4 + baseOffset;
        int minutesOffset = 5 + baseOffset;
        int secondsOffset = 6 + baseOffset;

        int year = characteristic.getIntValue(uInt16Format, yearOffset);
        int month = characteristic.getIntValue(uInt8Format, monthOffset);
        int day = characteristic.getIntValue(uInt8Format, dayOffset);
        int hour = characteristic.getIntValue(uInt8Format, hourOffset);
        int minutes = characteristic.getIntValue(uInt8Format, minutesOffset);
        int seconds = characteristic.getIntValue(uInt8Format, secondsOffset);
        Log.d(TAG, "{ year: " + year + ", month: " + month +
                ", day: " + day + ", hour:" + hour +
                ", minutes: " + minutes + ", seconds: " + seconds + "}");

        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day, hour, minutes, seconds);
        return calendar.getTime();
    }

}

package dk.silverbullet.device_integration.protocols.continua;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.protocols.continua.android.SingleShotTimer;
import dk.silverbullet.device_integration.protocols.continua.android.StopwatchListener;
import dk.silverbullet.device_integration.protocols.continua.packet.SystemId;
import dk.silverbullet.device_integration.protocols.continua.packet.output.OutputPacket;
import dk.silverbullet.device_integration.protocols.continua.protocol.ProtocolStateListener;
import dk.silverbullet.device_integration.listeners.DeviceListener;

/**
 * Superclass with all the common functionality of the actual Continua controllers.
 */
public abstract class AbstractDeviceController<MeasurementType>
        implements ContinuaDeviceController, HdpListener, ProtocolStateListener<MeasurementType> {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AbstractDeviceController.class);
    protected final DeviceListener<MeasurementType> listener;
    protected final HdpController hdpController;

    // --*-- Constructors --*--

    protected AbstractDeviceController(DeviceListener<MeasurementType> listener,
                                       HdpController hdpController,
                                       HdpProfile hdpProfile) {
        this.listener = listener;
        this.hdpController = hdpController;

        hdpController.setHdpProfile(hdpProfile);
        hdpController.setBluetoothListener(this);
    }

    // --*-- Methods --*--

    @Override
    public void close() {
        Log.d(TAG, "close!");
        hdpController.terminate();
    }

    @Override
    public void applicationConfigurationRegistrationFailed() {
        Log.d(TAG, "applicationConfigurationRegistrationFailed!");
        listener.permanentProblem();
    }

    @Override
    public void applicationConfigurationRegistered() {
        Log.d(TAG, "applicationConfigurationRegistered");
    }

    @Override
    public void applicationConfigurationUnregistered() {
        Log.d(TAG, "applicationConfigurationUnregistered");
    }

    @Override
    public void applicationConfigurationUnregistrationFailed() {
        Log.d(TAG, "applicationConfigurationUnregistrationFailed");
    }

    @Override
    public void serviceConnectionFailed() {
        Log.d(TAG, "serviceConnectionFailed!");
        listener.permanentProblem();
    }

    @Override
    public void connectionEstablished() {
        Log.d(TAG, "connectionEstablished!");
        listener.connected();
    }

    @Override
    public void disconnected() {
        Log.d(TAG, "disconnected!");
        listener.disconnected();
    }

    @Override
    public void measurementReceived(DeviceInformation deviceInformation, MeasurementType measurement) {
        Log.d(TAG, "measurementReceived!");
        listener.measurementReceived(deviceInformation, measurement);
    }

    @Override
    public void noMeasurementsReceived() {
        Log.d(TAG, "noMeasurementReceived!");
        listener.temporaryProblem();
    }

    @Override
    public void sendPacket(OutputPacket packet) throws IOException {
        Log.d(TAG, "Sending: " + packet.toString());
        hdpController.send(packet.getContents());
    }

    @Override
    public void tooManyRetries() {
        Log.d(TAG, "tooManyRetries!");
        listener.permanentProblem();
    }

    @Override
    public void finishNow() {
        Log.d(TAG, "finishNow!");
        close();
    }

    @Override
    public void finish() {
        Log.d(TAG, "finish!");
        new SingleShotTimer(1000, new StopwatchListener() {
            @Override
            public void timeout() {
                finishNow();
            }
        });
    }
}

package dk.silverbullet.device_integration.protocols.continua;

import java.io.IOException;
import java.util.regex.Pattern;

import dk.silverbullet.device_integration.exceptions.DeviceInitialisationException;

public interface HdpController {

    void setHdpProfile(HdpProfile bloodPressureMeter);

    void setPacketCollector(PacketCollector collector);

    void setBluetoothListener(HdpListener listener);

    void setPollForConnection(boolean pollForConnection);

    void initiate(Pattern deviceNamePattern, Pattern deviceMacAddressPrefixPattern) throws DeviceInitialisationException;

    void send(byte[] contents) throws IOException;

    void terminate();
}

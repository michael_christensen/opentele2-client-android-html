package dk.silverbullet.device_integration.protocols.bluetooth_smart;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import dk.silverbullet.device_integration.Util;

// TODO: Delete this
public class DefaultGattCallback extends BluetoothGattCallback implements Parcelable {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(DefaultGattCallback.class);

    private BluetoothLowEnergyService service;

    // --*-- Constructors --*--

    public DefaultGattCallback() {}

    // --*-- Methods --*--

    // -*- Callback methods -*-

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        Log.d(TAG, "onConnectionStateChange: " + status + ", " + newState);

        if (newState == BluetoothProfile.STATE_CONNECTED) {
            service.setConnectionState(GattConstants.STATE_CONNECTED);
            Log.i(TAG, "Connected to GATT server.");
            BluetoothGatt bluetoothGatt = service.getBluetoothGatt();
            Log.i(TAG, "Attempting to start service discovery: " +
                    bluetoothGatt.discoverServices());

        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            service.setConnectionState(GattConstants.STATE_DISCONNECTED);
            Log.i(TAG, "Disconnected from GATT server.");
        } else {
            Log.i(TAG, "State change: " + newState);
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        Log.i(TAG, "onServicesDiscovered: { status: " + status + " }");
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic,
                                     int status) {
        Log.i(TAG, "onCharacteristicRead: { status: " + status +
                ", characteristicUUID: " + characteristic.getUuid() + " }");
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt,
                                      BluetoothGattCharacteristic characteristic,
                                      int status) {
        Log.i(TAG, "onCharacteristicWrite: { status: " + status +
                ", characteristicUUID: " + characteristic.getUuid() + " }");
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt,
                                        BluetoothGattCharacteristic characteristic) {
        Log.i(TAG, "onCharacteristicRead: { characteristicUUID: " +
                characteristic.getUuid() + " }");
    }

    @Override
    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        Log.i(TAG, "onServicesDiscovered: { status: " + status + " }");
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        Log.i(TAG, "onServicesDiscovered: { status: " + status + " }");
    }

    @Override
    public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
        Log.i(TAG, "onServicesDiscovered: { status: " + status + " }");
    }

    @Override
    public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
        Log.i(TAG, "onServicesDiscovered: { status: " + status +
                ", rssi: " + rssi + " }");
    }

    @Override
    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
        Log.i(TAG, "onServicesDiscovered: { status: " + status +
                ", mtu: " + mtu + " }");
    }

    // -*- Characteristic parsing -*-

    public void tryToRegisterCharacteristic(int status,
                                            UUID serviceUUID,
                                            UUID characteristicUUID) {

        if (status == BluetoothGatt.GATT_SUCCESS) {
            Log.d(TAG, "Supported services:");

            for (BluetoothGattService gattService : getService().getSupportedGattServices()) {
                UUID aServiceUUID = gattService.getUuid();
                Log.d(TAG, "Service Id: " + aServiceUUID);

                if (aServiceUUID.equals(serviceUUID)) {
                    Log.d(TAG, "Found weight scale service!");

                    for (BluetoothGattCharacteristic characteristic : gattService.getCharacteristics()) {
                        UUID aCharacteristicUUID = characteristic.getUuid();
                        Log.d(TAG, "Characteristic UUID: " + aCharacteristicUUID);

                        if (aCharacteristicUUID.equals(characteristicUUID)) {
                            Log.d(TAG, "Found weight measurement characteristic!");

                            // TODO: Find out which one of these two to call.
                            // getService().readCharacteristic(characteristic);
                            getService().setCharacteristicNotification(characteristic, true);
                        }
                    }
                }
            }
        } else {
            Log.w(TAG, "onServicesDiscovered received: " + status);
        }
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public Date parseDate(BluetoothGattCharacteristic characteristic, int baseOffset) {

        int uInt16Format = BluetoothGattCharacteristic.FORMAT_UINT16;
        int uInt8Format = BluetoothGattCharacteristic.FORMAT_UINT8;

        int yearOffset = baseOffset;
        int monthOffset = 2 + baseOffset;
        int dayOffset = 3 + baseOffset;
        int hourOffset = 4 + baseOffset;
        int minutesOffset = 5 + baseOffset;
        int secondsOffset = 6 + baseOffset;

        int year = characteristic.getIntValue(uInt16Format, yearOffset);
        int month = characteristic.getIntValue(uInt8Format, monthOffset);
        int day = characteristic.getIntValue(uInt8Format, dayOffset);
        int hour = characteristic.getIntValue(uInt8Format, hourOffset);
        int minutes = characteristic.getIntValue(uInt8Format, minutesOffset);
        int seconds = characteristic.getIntValue(uInt8Format, secondsOffset);
        Log.d(TAG,"{ year: " + year + ", month: " + month +
                ", day: " + day +  ", hour:" + hour +
                ", minutes: " + minutes + ", seconds: " + seconds + "}");

        Calendar calendar = new GregorianCalendar();
        calendar.set(year, month, day, hour, minutes, seconds);
        return calendar.getTime();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {});
    }

    // -*- Getters/Setters -*-

    public BluetoothLowEnergyService getService() {
        return service;
    }

    public void setService(BluetoothLowEnergyService service) {
        this.service = service;
    }

}

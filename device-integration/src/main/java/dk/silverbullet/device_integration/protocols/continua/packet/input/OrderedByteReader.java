package dk.silverbullet.device_integration.protocols.continua.packet.input;

import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.exceptions.EndOfFileException;

public class OrderedByteReader {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(OrderedByteReader.class);

    private final ByteArrayInputStream inputStream;

    // --*-- Constructors --*--

    public OrderedByteReader(byte[] data) {
        this.inputStream = new ByteArrayInputStream(data);
    }

    // --*-- Methods --*--

    public int readByte() throws EndOfFileException {
        int value = inputStream.read();
        if (value < 0) {
            throw new EndOfFileException();
        }
        return value;
    }

    public int readShort() throws EndOfFileException {
        int a = readByte();
        int b = readByte();
        return (a << 8) | b;
    }

    public int readInt() throws EndOfFileException {
        int a = readShort();
        int b = readShort();
        return (a << 16) | b;
    }

    public long readLong() throws EndOfFileException {
        long a = readInt();
        long b = readInt();
        return (a << 32) | (b & 0xffffffffL);
    }

    public void close() {
        Log.d(TAG, "OrderedByteReader close!");
        try {
            inputStream.close();
        } catch (IOException e) {
            throw new RuntimeException("Could not close underlying stream", e);
        }
    }

    public int available() {
        return inputStream.available();
    }
}

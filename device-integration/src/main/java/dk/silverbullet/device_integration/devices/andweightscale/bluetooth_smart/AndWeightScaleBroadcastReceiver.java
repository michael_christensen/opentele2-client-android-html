package dk.silverbullet.device_integration.devices.andweightscale.bluetooth_smart;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.io.Serializable;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andweightscale.Weight;
import dk.silverbullet.device_integration.listeners.DeviceListener;
import dk.silverbullet.device_integration.protocols.bluetooth_smart.GattConstants;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;

public class AndWeightScaleBroadcastReceiver extends BroadcastReceiver {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AndWeightScaleBroadcastReceiver.class);

    private final DeviceListener<Weight> weightDeviceListener;

    // --*-- Constructors --*--

    public AndWeightScaleBroadcastReceiver(DeviceListener<Weight> weightDeviceListener) {
        this.weightDeviceListener = weightDeviceListener;
    }

    // --*-- Methods --*--

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        switch (action) {
            case GattConstants.CONNECTED:
                connected();
                break;

            case GattConstants.DISCONNECTED:
                disconnected();
                break;

            case GattConstants.SERVICES_DISCOVERED:
                servicesDiscovered();
                break;

            case GattConstants.DATA_AVAILABLE:
                dataAvailable(intent);
                break;

            default:
                Log.w(TAG, "Unknown event caught: " + action);
                break;
        }
    }

    private void connected() {
        Log.d(TAG, "CONNECTED");
        weightDeviceListener.connected();
    }

    private void disconnected() {
        Log.d(TAG, "DISCONNECTED");
        weightDeviceListener.disconnected();
    }

    private void servicesDiscovered () {
        Log.d(TAG, "SERVICES_DISCOVERED");
    }

    private void dataAvailable(Intent intent) {
        Log.d(TAG, "DATA_AVAILABLE");
        Serializable intentContent = intent.getSerializableExtra(GattConstants.DATA_AVAILABLE);
        // TODO do something else so that we can call
        // weightDeviceListener.measurementReceived(deviceInformation, weight)
        // get a hold of the service and call .close()
        if (intentContent instanceof Weight) {
            Weight weight = (Weight) intentContent;
            Log.d(TAG, "Received weight: " + weight.toString());
        } else if (intentContent instanceof DeviceInformation) {
            DeviceInformation deviceInformation = (DeviceInformation) intentContent;
            Log.d(TAG, "Received device information: " + deviceInformation.toString());
        } else {
            Log.d(TAG, "Received something else: " + intentContent.toString());
        }
    }

}

package dk.silverbullet.device_integration.devices.andweightscale.continua.protocol;

import android.util.Log;

import java.io.IOException;

import dk.silverbullet.device_integration.Util;
import dk.silverbullet.device_integration.devices.andweightscale.Weight;
import dk.silverbullet.device_integration.devices.andweightscale.continua.packet.input.AndWeightScaleConfirmedMeasurementDataPacket;
import dk.silverbullet.device_integration.protocols.continua.DeviceInformation;
import dk.silverbullet.device_integration.protocols.continua.packet.input.AssociationReleaseRequestPacket;
import dk.silverbullet.device_integration.protocols.continua.protocol.ProtocolStateController;
import dk.silverbullet.device_integration.protocols.continua.protocol.ProtocolStateListener;

/**
 * Implementation of the protocol specific for the A&amp;D weight scale.
 */
public class AndWeightScaleProtocolStateController extends
        ProtocolStateController<Weight, AndWeightScaleConfirmedMeasurementDataPacket> {

    // --*-- Fields --*--

    private static final String TAG = Util.getTag(AndWeightScaleProtocolStateController.class);

    private static final int NO_TIME = -1;
    private long timestamp = NO_TIME;
    private Weight weight;
    private Weight.Unit unit;
    private DeviceInformation deviceInformation;

    // --*-- Constructors --*--

    public AndWeightScaleProtocolStateController(ProtocolStateListener<Weight> listener) {
        super(listener);
    }

    // --*-- Methods --*--

    @Override
    protected AndWeightScaleConfirmedMeasurementDataPacket createConfirmedMeasurementsType(byte[] contents)
            throws IOException {
        return new AndWeightScaleConfirmedMeasurementDataPacket(contents);
    }

    @Override
    protected void handleConfirmedMeasurement(AndWeightScaleConfirmedMeasurementDataPacket confirmedMeasurement) {
        Log.d(TAG, "ConfirmedMeasurement: " + confirmedMeasurement);

        if (confirmedMeasurement.getWeight() != null) {
            Weight weight = confirmedMeasurement.getWeight();
            if (weight.getUnit() != null) {
                unit = weight.getUnit();
            }
        }

        if (confirmedMeasurement.hasDeviceInformation()) {
            deviceInformation = confirmedMeasurement.getDeviceInformation();
            deviceInformation.setSystemId(systemId.asString());
            Log.d(TAG, "Setting deviceInformation on WeightScaleStateController: " + deviceInformation);
        }

        if (confirmedMeasurement.getTimestamp() > timestamp) {
            weight = confirmedMeasurement.getWeight();

            if (weight.getUnit() == null) {
                weight = new Weight(weight.getWeight(), unit);
            }

            if (weight.getWeight() < 0) {
                weight = null;
            }

            timestamp = confirmedMeasurement.getTimestamp();
        }
    }

    @Override
    protected void handleAssociationReleaseRequest(AssociationReleaseRequestPacket associationReleaseRequestPacket) {

        if (deviceInformation == null) {
            deviceInformation = new DeviceInformation();
            deviceInformation.setSystemId(systemId.asString());
        }

        if (weight != null) {
            listener.measurementReceived(deviceInformation, weight);
        } else {
            listener.noMeasurementsReceived();
        }
    }
}

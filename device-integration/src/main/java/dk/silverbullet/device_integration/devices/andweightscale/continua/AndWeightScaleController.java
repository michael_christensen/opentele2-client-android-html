package dk.silverbullet.device_integration.devices.andweightscale.continua;

import java.util.regex.Pattern;

import dk.silverbullet.device_integration.devices.andweightscale.Weight;
import dk.silverbullet.device_integration.devices.andweightscale.continua.protocol.AndWeightScaleProtocolStateController;
import dk.silverbullet.device_integration.listeners.DeviceListener;
import dk.silverbullet.device_integration.exceptions.DeviceInitialisationException;
import dk.silverbullet.device_integration.protocols.continua.AbstractDeviceController;
import dk.silverbullet.device_integration.protocols.continua.HdpController;
import dk.silverbullet.device_integration.protocols.continua.HdpProfile;
import dk.silverbullet.device_integration.protocols.continua.PacketCollector;

/**
 * Main entry point to communicating with the A&D Medical weight scale.
 */
public class AndWeightScaleController extends AbstractDeviceController<Weight> {

    // --*-- Constructors --*--

    /**
     * @param listener Object given callbacks for e.g. notifying the user about
     *                 the progress through the GUI.
     * @param hdpController
     *            The specific Bluetooth controller to use for the underlying
     *            Bluetooth communication. See {@see AndroidBluetoothController}.
     */
    public AndWeightScaleController(DeviceListener<Weight> listener,
                                     HdpController hdpController) {
        super(listener, hdpController, HdpProfile.BODY_WEIGHT_SCALE);

        hdpController.setPacketCollector(new PacketCollector(
                new AndWeightScaleProtocolStateController(this)));
    }

    // --*-- Methods --*--

    /**
     * @param deviceNamePattern -
     * @param macAddressPattern -
     *
     * @throws DeviceInitialisationException if Bluetooth is not available, not
     * enabled, or if a general Bluetooth error occurs.
     */
    @Override
    public void initiate(Pattern deviceNamePattern, Pattern macAddressPattern)
            throws DeviceInitialisationException {
        listener.connecting();
        hdpController.initiate(deviceNamePattern, macAddressPattern);
    }

}

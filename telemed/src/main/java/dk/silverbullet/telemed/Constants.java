package dk.silverbullet.telemed;

public final class Constants {

    // --*-- Constants --*--

    public static final String ERROR_URL = "file:///android_asset/www/error-page.html";
    public static final String OPEN_TELE_HTML5_URL = "OpenTeleUrl";
    public static final String VIDEO_PROVIDER_QUALIFIED_NAME = "dk.silverbullet.telemed.video.VideoProvider";

    // --*-- Constructors --*--

    private Constants() {
        throw new RuntimeException("Constants cannot be instantiated.");
    }

}

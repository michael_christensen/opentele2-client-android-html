package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;
import dk.silverbullet.telemed.video.VideoConference;

public class StartVideoConferenceRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;

    public StartVideoConferenceRequestHandler(MainActivity mainActivity) {

        this.mainActivity = mainActivity;
    }

    @Override
    public String getMessageTypeHandled() {
        return "startVideoConferenceRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {

        String conferenceDetails = message.get("conferenceDetails").toString();

        VideoConference videoConference = VideoConference.fromJson(conferenceDetails);
        mainActivity.joinConference(videoConference);

    }
}

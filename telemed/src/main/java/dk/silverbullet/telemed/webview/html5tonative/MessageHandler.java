package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

public interface MessageHandler {

    String getMessageTypeHandled();

    void consume(JSONObject message) throws JSONException;
}

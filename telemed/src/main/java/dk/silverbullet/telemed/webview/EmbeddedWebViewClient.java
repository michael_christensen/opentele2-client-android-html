package dk.silverbullet.telemed.webview;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import dk.silverbullet.telemed.Constants;
import dk.silverbullet.telemed.Util;
import dk.silverbullet.telemed.webview.nativetohtml5.LifeCycleEvents;

/**
 * Embeds the content of a URL into a View.
 *
 * @author Peter Urbak
 * @version 2014-10-14
 */
public class EmbeddedWebViewClient extends WebViewClient {

    private static final String TAG = Util.getTag(EmbeddedWebViewClient.class);
    private AppLoaded webViewCallback;

    public void setWebViewCallback(AppLoaded callback) {
        webViewCallback = callback;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        webView.loadUrl(url);
        return true;
    }

    @Override
    public void onReceivedError(WebView webView, int errorCode,
                                String description, String failingUrl) {
        try {
            webView.stopLoading();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (webView.canGoBack()) {
            webView.goBack();
        }

        webView.loadUrl(Constants.ERROR_URL);
        super.onReceivedError(webView, errorCode, description, failingUrl);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        Log.d(TAG, "Finished loading page: " + url);
        if (webViewCallback != null) {
            webViewCallback.loaded(url);
        }
    }
}

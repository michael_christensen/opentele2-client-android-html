package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.MainActivity;

public class StartNotificationSoundRequestHandler implements MessageHandler {

    private final MainActivity mainActivity;

    public StartNotificationSoundRequestHandler(MainActivity mainActivity) {

        this.mainActivity = mainActivity;
    }

    @Override
    public String getMessageTypeHandled() {
        return "startNotificationSoundRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {
        mainActivity.setVideoInProgress(true);
        mainActivity.playNotificationSound();
    }
}

package dk.silverbullet.telemed.webview.html5tonative;

import org.json.JSONException;
import org.json.JSONObject;

import dk.silverbullet.telemed.devices.DeviceHandler;

public class DeviceMeasurementRequestHandler implements MessageHandler{

    private final DeviceHandler deviceHandler;

    public DeviceMeasurementRequestHandler(DeviceHandler deviceHandler) {

        this.deviceHandler = deviceHandler;
    }

    @Override
    public String getMessageTypeHandled() {
        return "deviceMeasurementRequest";
    }

    @Override
    public void consume(JSONObject message) throws JSONException {

        String measurementType = message.getString("measurementType");
        deviceHandler.addDeviceListener(measurementType);
    }
}

package dk.silverbullet.telemed;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public final class Util {

    // --*-- Fields --*--

    private static final String TAG = getTag(Util.class);

    // Device information
    private static final String BRAND = "brand";
    private static final String MODEL = "model";
    private static final String SERIAL = "serial";
    private static final String ANDROID_VERSION = "androidVersion";
    private static final String TOTAL_MEMORY = "totalMemory";
    private static final String AVAILABLE_MEMORY = "availableMemory";
    private static final String BLUETOOTH = "bluetooth";
    private static final String WIFI = "wifi";

    // --*-- Constructors --*--

    private Util() {
        throw new RuntimeException("Util cannot be instantiated.");
    }

    // --*-- Methods --*--

    public static String getTag(Class<?> cls) {
        return cls.getName().substring(cls.getName().lastIndexOf(".") + 1);
    }

    /**
     * Adjusts the screen orientation depending on whether it is a tablet or
     * a phone. Any device with a screen size greater than or equal to 720x960
     * dp units will be in landscape mode.
     */
    public static void adjustScreenOrientation(Activity activity) {
        Context context = activity.getApplicationContext();
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
        if (xlarge) {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    /**
     * @return a <code>Map</code> of device information.
     */
    @SuppressLint("NewApi")
    public static Map<String, String> getDeviceInformation(Activity activity) {
        Map<String, String> deviceMap = new HashMap<>();

        // General
        deviceMap.put(MODEL, Build.MODEL);
        deviceMap.put(SERIAL, Build.SERIAL);
        deviceMap.put(BRAND, Build.BRAND);
        deviceMap.put(ANDROID_VERSION, Build.VERSION.RELEASE);

        // Memory
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) activity.getSystemService(Activity.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(memoryInfo);
        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.JELLY_BEAN) {
            deviceMap.put(TOTAL_MEMORY, String.valueOf(memoryInfo.totalMem));
            deviceMap.put(AVAILABLE_MEMORY, String.valueOf(memoryInfo.availMem));
        }

        // Network
        deviceMap.put(BLUETOOTH, "N/A");
        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        deviceMap.put(WIFI, String.valueOf(wifiManager.isWifiEnabled()));

        return deviceMap;
    }

    @SuppressWarnings("ConstantConditions")
    public static String getPatientPrivacyPolicy(Activity activity) {
        AssetManager am = activity.getAssets();
        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(
                    am.open("patient_privacy_policy.txt")));
            StringBuilder stringBuilder = new StringBuilder();
            String read = bufferedReader.readLine();

            while (read != null) {
                stringBuilder.append(read);
                stringBuilder.append(System.getProperty("line.separator"));
                read = bufferedReader.readLine();
            }
            return stringBuilder.toString();

        } catch (IOException e) {
            Log.d(TAG, "Could not find a Patient privacy policy");
            return null;

        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
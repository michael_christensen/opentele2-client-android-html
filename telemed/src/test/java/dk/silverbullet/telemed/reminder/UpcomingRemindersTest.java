package dk.silverbullet.telemed.reminder;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.*;

public class UpcomingRemindersTest {

    // --*-- Fields --*--

    private Date now = time("2013-06-05 16:12:02");

    // --*-- Methods --*--

    private ReminderBean reminderBean(long questionnaireId, String questionnaireName, Long... alarms) {
        ReminderBean result = new ReminderBean();
        result.setQuestionnaireId(questionnaireId);
        result.setQuestionnaireName(questionnaireName);
        result.setAlarms(Arrays.asList(alarms));
        return result;
    }

    private long secondsTo(String timeAsString) {
        return (time(timeAsString).getTime() - now.getTime()) / 1000;
    }

    private Date time(String asString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        try {
            return format.parse(asString);
        } catch (ParseException e) {
            throw new RuntimeException("Could not parse '" + asString + "'", e);
        }
    }

    // --*-- Tests --*--

    @Test
    public void testHasNoUpcomingRemindersWhenReminderBeanListIsEmpty() {
        UpcomingReminders reminders = new UpcomingReminders(now, new ArrayList<ReminderBean>());
        assertFalse(reminders.hasMoreReminders());
    }

    @Test
    public void testKnowsUpcomingAlarmForOneReminderBean() {
        ReminderBean reminderBean = reminderBean(23, "First questionnaire", secondsTo("2013-06-05 17:00:00"));

        UpcomingReminders reminders = new UpcomingReminders(now, Collections.singletonList(reminderBean));
        assertTrue(reminders.hasMoreReminders());
        assertEquals(time("2013-06-05 17:00:00"), reminders.nextReminder());
    }

    @Test
    public void testHandlesUpcomingAlarmsInFarFuture() {
        Calendar farInTheFuture = Calendar.getInstance();
        farInTheFuture.add(Calendar.YEAR, 10);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        long secondsToFarFuture = secondsTo(format.format(farInTheFuture.getTime()));
        ReminderBean reminderBean = reminderBean(23, "First questionnaire", secondsToFarFuture);

        UpcomingReminders reminders = new UpcomingReminders(now, Collections.singletonList(reminderBean));
        assertTrue(reminders.hasMoreReminders());
        assertTrue(reminders.nextReminder().after(new Date()));
    }

    @Test
    public void testKnowsFirstUpcomingAlarmForSeveralReminderBeans() {
        ReminderBean reminderBean1 = reminderBean(23, "First questionnaire", secondsTo("2013-06-05 17:00:00"), secondsTo("2013-06-05 17:15:00"));
        ReminderBean reminderBean2 = reminderBean(23, "Second questionnaire", secondsTo("2013-06-05 16:15:00"), secondsTo("2013-06-05 16:30:00"));
        ReminderBean reminderBean3 = reminderBean(23, "Third questionnaire", secondsTo("2013-06-05 17:30:00"));

        UpcomingReminders reminders = new UpcomingReminders(now, Arrays.asList(reminderBean1, reminderBean2, reminderBean3));
        assertTrue(reminders.hasMoreReminders());
        assertEquals(time("2013-06-05 16:15:00"), reminders.nextReminder());
    }

    @Test
    public void testKnowsWhichQuestionnairesHaveHadAlarmsAtSpecificTime() {
        ReminderBean reminderBean1 = reminderBean(23, "First questionnaire", secondsTo("2013-06-05 17:00:00"), secondsTo("2013-06-05 17:15:00"));
        ReminderBean reminderBean2 = reminderBean(23, "Second questionnaire", secondsTo("2013-06-05 16:15:00"), secondsTo("2013-06-05 16:30:00"));
        ReminderBean reminderBean3 = reminderBean(23, "Third questionnaire", secondsTo("2013-06-05 17:30:00"));

        UpcomingReminders reminders = new UpcomingReminders(now, Arrays.asList(reminderBean1, reminderBean2, reminderBean3));

        assertTrue(reminders.remindedQuestionnairesAt(time("2013-06-05 16:14:00")).isEmpty());

        assertEquals(Collections.singletonList("Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 16:15:00")));
        assertEquals(Collections.singletonList("Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 16:59:00")));

        assertEquals(Arrays.asList("First questionnaire", "Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 17:00:00")));
        assertEquals(Arrays.asList("First questionnaire", "Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 17:29:00")));

        assertEquals(Arrays.asList("First questionnaire", "Second questionnaire", "Third questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 17:30:00")));
        assertEquals(Arrays.asList("First questionnaire", "Second questionnaire", "Third questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 19:00:00")));
    }

    @Test
    public void testCanRemoveRemindersBeforeOrAtSpecificTime() {
        ReminderBean reminderBean1 = reminderBean(23, "First questionnaire", secondsTo("2013-06-05 17:00:00"), secondsTo("2013-06-05 17:15:00"));
        ReminderBean reminderBean2 = reminderBean(23, "Second questionnaire", secondsTo("2013-06-05 16:15:00"), secondsTo("2013-06-05 16:30:00"));
        ReminderBean reminderBean3 = reminderBean(23, "Third questionnaire", secondsTo("2013-06-05 17:30:00"));

        UpcomingReminders reminders = new UpcomingReminders(now, Arrays.asList(reminderBean1, reminderBean2, reminderBean3));

        assertEquals(Collections.singletonList("Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 16:15:00")));
        reminders.removeRemindersBeforeOrAt(time("2013-06-05 16:15:00"));
        assertTrue(reminders.remindedQuestionnairesAt(time("2013-06-05 16:15:00")).isEmpty());
        assertTrue(reminders.hasMoreReminders());

        assertEquals(Collections.singletonList("Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 16:30:00")));
        reminders.removeRemindersBeforeOrAt(time("2013-06-05 16:30:00"));
        assertTrue(reminders.remindedQuestionnairesAt(time("2013-06-05 16:30:00")).isEmpty());
        assertTrue(reminders.hasMoreReminders());

        assertEquals(Collections.singletonList("First questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 17:00:00")));
        reminders.removeRemindersBeforeOrAt(time("2013-06-05 17:00:00"));
        assertTrue(reminders.remindedQuestionnairesAt(time("2013-06-05 17:00:00")).isEmpty());
        assertTrue(reminders.hasMoreReminders());

        assertEquals(Arrays.asList("First questionnaire", "Third questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 17:30:00")));
        reminders.removeRemindersBeforeOrAt(time("2013-06-05 17:30:00"));
        assertTrue(reminders.remindedQuestionnairesAt(time("2013-06-05 17:30:00")).isEmpty());
        assertFalse(reminders.hasMoreReminders());
    }

    @Test
    public void testCanRemoveRemindersForSpecificQuestionnaire() {
        ReminderBean reminderBean1 = reminderBean(23, "First questionnaire", secondsTo("2013-06-05 17:00:00"), secondsTo("2013-06-05 17:15:00"));
        ReminderBean reminderBean2 = reminderBean(23, "Second questionnaire", secondsTo("2013-06-05 16:15:00"), secondsTo("2013-06-05 16:30:00"));

        UpcomingReminders reminders = new UpcomingReminders(now, Arrays.asList(reminderBean1, reminderBean2));
        reminders.removeQuestionnaire("First questionnaire");
        assertEquals(Collections.singletonList("Second questionnaire"), reminders.remindedQuestionnairesAt(time("2013-06-05 17:30:00")));
    }

}
